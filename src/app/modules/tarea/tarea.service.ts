import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { createRequestParams } from 'src/app/utils/utils';
import { environment } from 'src/environments/environment';
import { ITarea } from './tarea';

@Injectable({
  providedIn: 'root'
})
export class TareaService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable <ITarea[]> {
    let params = createRequestParams(req);
    return this.http.get<ITarea[]>(`${environment.END_POINT}/api/tarea`, {params: params})
    .pipe(map(res => {
      return res;
    }));
  }

  public save(tarea: ITarea): Observable<ITarea>{
    return this.http.post<ITarea>(`${environment.END_POINT}/api/tarea`, tarea)
    .pipe(map(res =>{
      return res;
    }));
  }
  public getById(id :string): Observable<ITarea>{
    return this.http.get<ITarea>(`${environment.END_POINT}/api/tarea/${id}`)
  .pipe(map(res =>{
    return res;
  }));
}

public update(tarea: ITarea): Observable<ITarea>{
  return this.http.put<ITarea>(`${environment.END_POINT}/api/tarea`, tarea)
  .pipe(map(res =>{
    return res;
  }));
}

public delete(id: number): Observable<ITarea>{
  return this.http.delete<ITarea>(`${environment.END_POINT}/api/tarea/${id}`)
  .pipe(map(res =>{
    return res;
  }));
}
}
