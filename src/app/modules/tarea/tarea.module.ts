import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TareaRoutingModule } from './tarea-routing.module';
import { TareaCreateComponent } from './tarea-create/tarea-create.component';
import { TareaListComponent } from './tarea-list/tarea-list.component';
import { TareaUpdateComponent } from './tarea-update/tarea-update.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TareaCreateComponent,
    TareaListComponent,
    TareaUpdateComponent
  ],
  imports: [
    CommonModule,
    TareaRoutingModule,
    ReactiveFormsModule
  ]
})
export class TareaModule { }
