import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ITarea } from '../tarea';
import { TareaService } from '../tarea.service';

@Component({
  selector: 'app-tarea-update',
  templateUrl: './tarea-update.component.html',
  styleUrls: ['./tarea-update.component.styl']
})
export class TareaUpdateComponent implements OnInit {

  tarea: ITarea;

  tareaGroupUpdate = this.fb.group({
    id: [],
    nombre: [],
    descripcion: [],
    alias: [],
    estado: [],
    eliminado: [],
    fechaInicio:[],
    fechaFin:[],
    fechaActualizacion: [],
    fechaCreacion: [],
  });

  constructor(private fb: FormBuilder, private service: TareaService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.service.getById(id)
    .subscribe(res =>{
      this.loadFrom(res);
    });
  }

  private loadFrom(tarea: ITarea){
    this.tareaGroupUpdate.patchValue({
    id: tarea.id,
    nombre: tarea.nombre,
    descripcion: tarea.descripcion,
    alias: tarea.alias,
    estado: tarea.estado,
    eliminado: tarea.eliminado,
    fechaInicio: tarea.fechaInicio,
    fechaFin: tarea.fechaFin,
    fechaActualizacion: tarea.fechaActualizacion,
    fechaCreacion: tarea.fechaCreacion,
    });
  }

  tareaUpdate(): void{
    console.warn('res', this.tareaGroupUpdate.value);
    this.service.update(this.tareaGroupUpdate.value).subscribe(res =>{
      this.router.navigate(['./dashboard/tarea-list']);
      console.warn('res2', res);
    })
  }

}
