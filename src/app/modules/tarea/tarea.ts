export interface ITarea {
    id?: number;
    nombre?: string;
    descripcion?: string;
    alias?: string;
    estado?: boolean;
    eliminado?: boolean;
    fechaInicio?: Date;
    fechaFin?: Date;
    fechaActualizacion?: Date;
    fechaCreacion?: Date;
}
export class Tarea implements ITarea{
    id?: number;
    nombre?: string;
    descripcion?: string;
    alias?: string;
    estado?: boolean;
    eliminado?: boolean;
    fechaInicio?: Date;
    fechaFin?: Date;
    fechaActualizacion?: Date;
    fechaCreacion?: Date;
}

