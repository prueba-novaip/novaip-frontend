import { Component, OnInit } from '@angular/core';
import { ITarea } from '../tarea';
import { TareaService } from '../tarea.service';

@Component({
  selector: 'app-tarea-list',
  templateUrl: './tarea-list.component.html',
  styleUrls: ['./tarea-list.component.styl']
})
export class TareaListComponent implements OnInit {

  listaTareas: ITarea[] = []
  pageSize = 5;
  pageNumber = 0;
  totalRecords: any;
  page: any;
  constructor(private service: TareaService) { }

  ngOnInit(): void {
    this.service.query({
      pageSize: this.pageSize,
      pageNumber: this.pageNumber}
    ).subscribe((res: any) => {
      this.listaTareas = res.content;
      console.warn('res', this.listaTareas);
    });
  }

  delete(id:number):void{
    this.service.delete(id).subscribe();
    this.listaTareas = this.listaTareas.filter(c => c.id != id)
  }

}
