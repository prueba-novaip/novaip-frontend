import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TareaCreateComponent } from './tarea-create/tarea-create.component';
import { TareaListComponent } from './tarea-list/tarea-list.component';
import { TareaUpdateComponent } from './tarea-update/tarea-update.component';

const routes: Routes = [
  {
    path:'tarea-list',
    component:TareaListComponent
  },
  {
    path:'tarea-create',
    component:TareaCreateComponent
  },
  {
    path:'tarea-update/:id',
    component:TareaUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TareaRoutingModule { }
