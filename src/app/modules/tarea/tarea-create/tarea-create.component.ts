import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { TareaService } from '../tarea.service';

@Component({
  selector: 'app-tarea-create',
  templateUrl: './tarea-create.component.html',
  styleUrls: ['./tarea-create.component.styl']
})
export class TareaCreateComponent implements OnInit {

  tareaGroup = this.fb.group({
    nombre: [],
    descripcion: [],
    alias: [],
    estado: [],
    eliminado: [],
    fechaInicio:[],
    fechaFin:[],
    fechaActualizacion: [],
    fechaCreacion: [],
  });

  constructor(private fb: FormBuilder, private service: TareaService, private router: Router) { }


  ngOnInit(): void {
  }
  saveTarea(){
    console.warn('res', this.tareaGroup.value);
    this.service.save(this.tareaGroup.value).subscribe(res =>{
      this.router.navigate(['./dashboard/tarea-list']);
      console.warn('res2', res);
    })
  }

}
