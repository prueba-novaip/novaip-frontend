import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProyectoCreateComponent } from './proyecto-create/proyecto-create.component';
import { ProyectoListComponent } from './proyecto-list/proyecto-list.component';
import { ProyectoUpdateComponent } from './proyecto-update/proyecto-update.component';

const routes: Routes = [
  {
    path:'proyecto-list',
    component:ProyectoListComponent
  },
  {
    path:'proyecto-create',
    component:ProyectoCreateComponent
  },
  {
    path:'proyecto-update/:id',
    component:ProyectoUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProyectoRoutingModule { }
