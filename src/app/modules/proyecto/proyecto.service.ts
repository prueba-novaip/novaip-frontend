import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { createRequestParams } from 'src/app/utils/utils';
import { IProyecto } from './proyecto';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ProyectoService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable <IProyecto[]> {
    let params = createRequestParams(req);
    return this.http.get<IProyecto[]>(`${environment.END_POINT}/api/proyecto`, { params: params })
    .pipe(map(res => {
      return res;
    }));
  }

  public save(proyecto: IProyecto): Observable<IProyecto>{
    return this.http.post<IProyecto>(`${environment.END_POINT}/api/proyecto`,proyecto)
    .pipe(map(res =>{
      return res;
    }));
  }

  public getById(id :string): Observable<IProyecto>{
      return this.http.get<IProyecto>(`${environment.END_POINT}/api/proyecto/${id}`)
    .pipe(map(res =>{
      return res;
    }));
  }

  public update(proyecto: IProyecto): Observable<IProyecto>{
    return this.http.put<IProyecto>(`${environment.END_POINT}/api/proyecto`, proyecto)
    .pipe(map(res =>{
      return res;
    }));
  }

  public delete(id: number): Observable<IProyecto>{
    return this.http.delete<IProyecto>(`${environment.END_POINT}/api/proyecto/${id}`)
    .pipe(map(res =>{
      return res;
    }));
  }
}
