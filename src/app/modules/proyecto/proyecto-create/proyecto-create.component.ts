import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ITarea } from '../../tarea/tarea';
import { TareaService } from '../../tarea/tarea.service';
import { IProyecto } from '../proyecto';
import { ProyectoService } from '../proyecto.service';

@Component({
  selector: 'app-proyecto-create',
  templateUrl: './proyecto-create.component.html',
  styleUrls: ['./proyecto-create.component.styl']
})
export class ProyectoCreateComponent implements OnInit {

  proyecto: IProyecto;
  tareas: ITarea [] = [];
  proyectoGroup = this.fb.group({
    nombre: [],
    descripcion: [],
    alias: [],
    estado: [],
    eliminado: [],
    fechaInicio:[],
    fechaFin:[],
    fechaActualizacion: [],
    fechaCreacion: [],
    idTarea: []
  });

  pageSize = 5;
  pageNumber = 0;
  
  constructor(private fb: FormBuilder, private proyectoService: ProyectoService, private tareaService: TareaService,
    private router: Router) { }

  ngOnInit(): void {
    this.tareaService.query({
      pageSize: this.pageSize,
      pageNumber: this.pageNumber}
    ).subscribe((res: any) => {
      this.tareas = res.content;
      console.warn('tarea', this.tareas);
    });
  }

  saveProyecto(){
    console.warn('res', this.proyectoGroup.value);
    this.proyectoService.save(this.proyectoGroup.value).subscribe(res =>{
      this.router.navigate(['./dashboard/proyecto-list']);
      console.warn('res2', res);
    })
  }

}
