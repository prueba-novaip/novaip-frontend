import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IProyecto } from '../proyecto';
import { ProyectoService } from '../proyecto.service';

@Component({
  selector: 'app-proyecto-list',
  templateUrl: './proyecto-list.component.html',
  styleUrls: ['./proyecto-list.component.styl']
})
export class ProyectoListComponent implements OnInit {

  listaProyecto: IProyecto[] = []
  pageSize = 2;
  pageNumber = 0;
  totalRecords: any;
  page: any;
  filter = {};
  constructor(private proyectoService: ProyectoService, private activatedRoute: ActivatedRoute ) { }

  ngOnInit(): void {
    this.proyectoService.query({
      pageSize: this.pageSize,
      pageNumber: this.pageNumber}
    ).subscribe((res: any) => {
      this.listaProyecto = res.content;
      console.warn('res', this.listaProyecto);
    });
  }

  delete(id:number):void{
    this.proyectoService.delete(id).subscribe();
    this.listaProyecto = this.listaProyecto.filter(c => c.id != id);
  }


}
