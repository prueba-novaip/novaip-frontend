import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IProyecto } from '../proyecto';
import { ProyectoService } from '../proyecto.service';

@Component({
  selector: 'app-proyecto-update',
  templateUrl: './proyecto-update.component.html',
  styleUrls: ['./proyecto-update.component.styl']
})
export class ProyectoUpdateComponent implements OnInit {

  proyecto: IProyecto;

  proyectoGroupUpdate = this.fb.group({
    id: [],
    nombre: [],
    descripcion: [],
    alias: [],
    estado: [],
    eliminado: [],
    fechaInicio:[],
    fechaFin:[],
    fechaActualizacion: [],
    fechaCreacion: [],
    idTarea: []
  });
  constructor(private fb: FormBuilder, private service: ProyectoService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.service.getById(id)
    .subscribe(res =>{
      this.loadFrom(res);
    });
  }

  private loadFrom(proyecto: IProyecto){
    this.proyectoGroupUpdate.patchValue({
    id: proyecto.id,
    nombre: proyecto.nombre,
    descripcion: proyecto.descripcion,
    alias: proyecto.alias,
    estado: proyecto.estado,
    eliminado: proyecto.eliminado,
    fechaInicio: proyecto.fechaInicio,
    fechaFin: proyecto.fechaFin,
    fechaActualizacion: proyecto.fechaActualizacion,
    fechaCreacion: proyecto.fechaCreacion,
    idTarea: proyecto.idTarea
    });
  }

  proyectoUpdate(): void{
    console.warn('res', this.proyectoGroupUpdate.value);
    this.service.update(this.proyectoGroupUpdate.value).subscribe(res =>{
      this.router.navigate(['./dashboard/proyecto-list']);
      console.warn('res2', res);
    })
  }

}
