export interface IProyecto {
    id?: number;
    nombre?: string;
    descripcion?: string;
    alias?: string;
    estado?: boolean;
    eliminado?: boolean;
    fechaInicio?: string;
    fechaFin?: Date;
    fechaActualizacion?: Date;
    fechaCreacion?: Date;
    idTarea?: number;
}
export class Proyecto implements IProyecto{
    id?: number;
    nombre?: string;
    descripcion?: string;
    alias?: string;
    estado?: boolean;
    eliminado?: boolean;
    fechaInicio?: string;
    fechaFin?: Date;
    fechaActualizacion?: Date;
    fechaCreacion?: Date;
    idTarea?: number;
}
