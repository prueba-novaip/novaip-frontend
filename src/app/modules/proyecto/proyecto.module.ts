import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProyectoRoutingModule } from './proyecto-routing.module';
import { ProyectoUpdateComponent } from './proyecto-update/proyecto-update.component';
import { ProyectoListComponent } from './proyecto-list/proyecto-list.component';
import { ProyectoCreateComponent } from './proyecto-create/proyecto-create.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ProyectoUpdateComponent,
    ProyectoListComponent,
    ProyectoCreateComponent,
  ],
  imports: [
    CommonModule,
    ProyectoRoutingModule,
    ReactiveFormsModule
  ]
})
export class ProyectoModule { }
