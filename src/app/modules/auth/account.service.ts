import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Account } from './models/account.model';
import { Observable, of, ReplaySubject } from 'rxjs';
import { catchError, shareReplay, tap } from 'rxjs/operators';
import { createRequestParams } from 'src/app/utils/utils';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private authenticationState = new ReplaySubject<Account | null>(1);
  private userIdentity: Account | null = null;
  private accountCache?: Observable<Account | null>;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  identity(force?: boolean): Observable<Account | null> {
    if (!this.accountCache || force || !this.isAuthenticated()) {
      this.accountCache = this.fetch().pipe(catchError(() => {
        return of(null);
      }),
        tap((account: Account | null) => {
          this.authenticate(account);
          console.warn('account', this.authenticate(account));
          if (account) {
            this.router.navigate(['dashboard']);
          }
        }),
        shareReplay()

      );
    }
    return this.accountCache;
  }

  isAuthenticated(): boolean {
    return this.userIdentity !== null;
  }

  authenticate(identity: Account | null): void {
    this.userIdentity = identity;
  }

  hasAnyAuthority(authorities: string[] | string): boolean {
    if (!this.userIdentity || !this.userIdentity.authorities) {
      return false;
    }
    if (!Array.isArray(authorities)) {

      authorities = [authorities];
    }
    return this.userIdentity.authorities.some((authority: string) => authorities.includes(authority));
  }

  getAuthenticationState(): Observable<Account | null> {
    return this.authenticationState.asObservable();
  }

  private fetch(): Observable<Account> {
    const jwt = localStorage.getItem('token') || sessionStorage.getItem('token');
    let params: any;

    if (jwt) {
      const payload: any = JSON.parse(atob(jwt.split('.')[1]));
      params = createRequestParams({ username: payload.user_name });
    } else {
      params = null;
    }
    return this.http.get<Account>(`${environment.END_POINT}/api/usuario/account`, { params: params });
    
  }

  getDocumentNumber(): string{
    return this.userIdentity.cedula;
  }
  getAuthorities(): string{
    var aut;
    aut = this.userIdentity.authorities[0].split("_");
    aut = aut[1];
    return aut;
  }
  getId(): number{
    return this.userIdentity.idUser;
  }
}
