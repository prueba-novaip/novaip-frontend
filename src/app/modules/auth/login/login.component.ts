import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.styl']
})
export class LoginComponent implements OnInit {

  loginForm = this.fb.group({
    email: ['', Validators.compose([
      Validators.required,
      Validators.pattern('^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$')
    ])],
    password: [''],
    remmemberMe: false
  });

  constructor(private fb: FormBuilder,
    private router: Router,
    private loginService: LoginService) { }

  ngOnInit(): void {
  }
  login(): void {
    this.loginService.login(this.loginForm.value)
    .subscribe((res: any) => {
    }, error =>{
      window.alert("Usuario y/o contraseña incorrectos")
    });

  }

}
