export interface ICredentials{
    email?: string,
    password?: string,
    remmemberMe?: boolean
  }
  
  export class Credentials implements ICredentials{
    constructor(
    public email?: string,
    public password?: string,
    public remmemberMe?: boolean
    ){}
  }