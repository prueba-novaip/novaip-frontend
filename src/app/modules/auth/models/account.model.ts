export class Account{
    constructor(  
      public idUser: number,
      public estado: boolean,
      public authorities: string[],
      public email: string,
      public nombre: string ,
      public cedula: string,
      public rols: string
    ){}
  }