import { IProyecto } from "../proyecto/proyecto";
import { IRols } from "../rols/rols";
import { ITarea } from "../tarea/tarea";

export interface IUsuario {
    id?: number;
    cedula?: string;
    nombre?: string;
    email?: string;
    contraseña?: string;
    estado?: boolean;
    eliminado?: boolean;
    fechaCreacion?: Date;
    fechaActualizacion?: Date;
    proyecto?: IProyecto;
    rols?: IRols;
    tarea?: ITarea;
   
}
