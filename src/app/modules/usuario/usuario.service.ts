import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { createRequestParams } from 'src/app/utils/utils';
import { environment } from 'src/environments/environment';
import { IUsuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable <IUsuario[]> {
    let params = createRequestParams(req);
    return this.http.get<IUsuario[]>(`${environment.END_POINT}/api/usuario`, {params: params})
    .pipe(map(res => {
      return res;
    }));
  }

  public save(usuario: IUsuario): Observable<IUsuario>{
    return this.http.post<IUsuario>(`${environment.END_POINT}/api/usuario`, usuario)
    .pipe(map(res =>{
      return res;
    }));
  }
  public getById(id :string): Observable<IUsuario>{
    return this.http.get<IUsuario>(`${environment.END_POINT}/api/usuario/${id}`)
  .pipe(map(res =>{
    return res;
  }));
}

public update(usuario: IUsuario): Observable<IUsuario>{
  return this.http.put<IUsuario>(`${environment.END_POINT}/api/usuario`, usuario)
  .pipe(map(res =>{
    return res;
  }));
}

public delete(id: number): Observable<IUsuario>{
  return this.http.delete<IUsuario>(`${environment.END_POINT}/api/usuario/${id}`)
  .pipe(map(res =>{
    return res;
  }));
}
}
