import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsuarioCreateComponent } from './usuario-create/usuario-create.component';
import { UsuarioListComponent } from './usuario-list/usuario-list.component';
import { UsuarioUpdateComponent } from './usuario-update/usuario-update.component';

const routes: Routes = [
  {
    path:'usuario-list',
    component:UsuarioListComponent
  },
  {
    path:'usuario-create',
    component:UsuarioCreateComponent
  },
  {
    path:'usuario-update/:id',
    component:UsuarioUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
