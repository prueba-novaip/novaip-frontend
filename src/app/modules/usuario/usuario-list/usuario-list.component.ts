import { Component, OnInit } from '@angular/core';
import { IUsuario } from '../usuario';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-usuario-list',
  templateUrl: './usuario-list.component.html',
  styleUrls: ['./usuario-list.component.styl']
})
export class UsuarioListComponent implements OnInit {

  listaUsuarios: IUsuario[] = []
  pageSize = 5;
  pageNumber = 0;
  totalRecords: any;
  page: any;

  constructor(private service: UsuarioService) { }

  ngOnInit(): void {
    this.service.query({
      pageSize: this.pageSize,
      pageNumber: this.pageNumber}
    ).subscribe((res: any) => {
      this.listaUsuarios = res.content;
      console.warn('res', this.listaUsuarios);
    });
  }

  delete(id:number):void{
    this.service.delete(id).subscribe();
    this.listaUsuarios = this.listaUsuarios.filter(c => c.id != id)
  }

}
