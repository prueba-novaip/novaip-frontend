import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { IProyecto } from '../../proyecto/proyecto';
import { ProyectoService } from '../../proyecto/proyecto.service';
import { IRols } from '../../rols/rols';
import { RolsService } from '../../rols/rols.service';
import { ITarea } from '../../tarea/tarea';
import { TareaService } from '../../tarea/tarea.service';
import { IUsuario } from '../usuario';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-usuario-create',
  templateUrl: './usuario-create.component.html',
  styleUrls: ['./usuario-create.component.styl']
})
export class UsuarioCreateComponent implements OnInit {

  usuario: IUsuario;
  rols: IRols [] = [];
  proyectos: IProyecto [] = [];
  tareas: ITarea [] = [];
  usuarioGroup = this.fb.group({
    cedula: [],
    nombre: [],
    email: [],
    contraseña: [],
    estado: [],
    eliminado: [],
    fechaCreacion: [],
    fechaActualizacion: [],
    proyecto: [],
    rols: [],
    tarea: [],
  })
  pageSize = 5;
  pageNumber = 0;
  constructor(private fb: FormBuilder, private service: UsuarioService, private rolsService: RolsService,
     private tareaService: TareaService, private proyectoService: ProyectoService, private router: Router) { }

  ngOnInit(): void {
    this.rolsService.query().subscribe(res =>{
      this.rols = res;
      console.warn('rols',res);
    })
    this.tareaService.query({
      pageSize: this.pageSize,
      pageNumber: this.pageNumber}
    ).subscribe((res: any) => {
      this.tareas = res.content;
      console.warn('tarea', this.tareas);
    });
    this.proyectoService.query({
      pageSize: this.pageSize,
      pageNumber: this.pageNumber}
    ).subscribe((res: any) => {
      this.proyectos = res.content;
      console.warn('proyecto', this.proyectos);
    });
  }
  
  saveUsuario(){
    console.warn('res', this.usuarioGroup.value);
    this.service.save(this.usuarioGroup.value).subscribe(res =>{
      console.warn('res2', res);
      this.router.navigate(['./dashboard/usuario-list']);

    })
  }

}
