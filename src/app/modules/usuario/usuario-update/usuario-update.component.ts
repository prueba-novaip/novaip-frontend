import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IProyecto } from '../../proyecto/proyecto';
import { ProyectoService } from '../../proyecto/proyecto.service';
import { IRols } from '../../rols/rols';
import { RolsService } from '../../rols/rols.service';
import { ITarea } from '../../tarea/tarea';
import { TareaService } from '../../tarea/tarea.service';
import { IUsuario } from '../usuario';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-usuario-update',
  templateUrl: './usuario-update.component.html',
  styleUrls: ['./usuario-update.component.styl']
})
export class UsuarioUpdateComponent implements OnInit {

  usuario: IUsuario;
  rols: IRols [] = [];
  proyectos: IProyecto [] = [];
  tareas: ITarea [] = [];
  usuarioGroupUpdate = this.fb.group({
    id: [],
    cedula: [],
    nombre: [],
    email: [],
    contraseña: [],
    estado: [],
    eliminado: [],
    fechaCreacion: [],
    fechaActualizacion: [],
    proyecto: [],
    rols: [],
    tarea: [],
    idRols:[],
    idTarea:[],
    idProyecto:[]
  })
  pageSize = 5;
  pageNumber = 0;
  constructor(private fb: FormBuilder, private service: UsuarioService, private activatedRoute: ActivatedRoute, private rolsService: RolsService,
    private proyectoService: ProyectoService, private tareaService: TareaService, private router: Router ) { }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.service.getById(id)
    .subscribe(res =>{
      this.loadFrom(res);
      console.warn("usuarios", res)
    });
    this.rolsService.query().subscribe(res =>{
      this.rols = res;
      console.warn('rols',res);
    })
    this.tareaService.query({
      pageSize: this.pageSize,
      pageNumber: this.pageNumber}
    ).subscribe((res: any) => {
      this.tareas = res.content;
      console.warn('tarea', this.tareas);
    });
    this.proyectoService.query({
      pageSize: this.pageSize,
      pageNumber: this.pageNumber}
    ).subscribe((res: any) => {
      this.proyectos = res.content;
      console.warn('proyecto', this.proyectos);
    });
  }

  private loadFrom(usuario: IUsuario){
    this.usuarioGroupUpdate.patchValue({
    id: usuario.id,
    nombre: usuario.nombre,
    cedula: usuario.cedula,
    email: usuario.email,
    estado: usuario.estado,
    eliminado: usuario.eliminado,
    fechaCreacion: usuario.fechaCreacion,
    contraseña: usuario.contraseña,
    fechaActualizacion: usuario.fechaActualizacion,
    proyecto:[
      usuario.proyecto.id = this.usuarioGroupUpdate.value.idProyecto
    ],
    rols: [
      usuario.rols.id = this.usuarioGroupUpdate.value.idRols,
    ],
    tarea:[
      usuario.tarea.id = this.usuarioGroupUpdate.value.idTarea,
    ] 
    });
  }

  updateUsuario(): void{
    console.warn('res', this.usuarioGroupUpdate.value);
    this.service.update(this.usuarioGroupUpdate.value).subscribe(res =>{
      console.warn('res2', res);
      this.router.navigate(['./dashboard/usuario-list']);

    })
  }

}
