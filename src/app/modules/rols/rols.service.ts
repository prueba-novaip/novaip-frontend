import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { IRols } from './rols';

@Injectable({
  providedIn: 'root'
})
export class RolsService {

  constructor(private http: HttpClient) { }

  public query(): Observable<IRols[]>{
    return this.http.get<IRols[]>(`${environment.END_POINT}/api/rols`)
    .pipe(map(res => {
      return res;
    }));
  }

  public save(rols: IRols): Observable<IRols>{
    return this.http.post<IRols>(`${environment.END_POINT}/api/rols`, rols)
    .pipe(map(res =>{
      return res;
    }));
  }
  public getById(id :string): Observable<IRols>{
    return this.http.get<IRols>(`${environment.END_POINT}/api/rols${id}`)
  .pipe(map(res =>{
    return res;
  }));
}

public update(rols: IRols): Observable<IRols>{
  return this.http.put<IRols>(`${environment.END_POINT}/api/rols`, rols)
  .pipe(map(res =>{
    return res;
  }));
}

public delete(id: string): Observable<IRols>{
  return this.http.delete<IRols>(`${environment.END_POINT}/api/rols${id}`)
  .pipe(map(res =>{
    return res;
  }));
}
}
