import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolsRoutingModule } from './rols-routing.module';
import { RolsCreateComponent } from './rols-create/rols-create.component';
import { RolsListComponent } from './rols-list/rols-list.component';
import { RolsUpdateComponent } from './rols-update/rols-update.component';


@NgModule({
  declarations: [
    RolsCreateComponent,
    RolsListComponent,
    RolsUpdateComponent
  ],
  imports: [
    CommonModule,
    RolsRoutingModule
  ]
})
export class RolsModule { }
