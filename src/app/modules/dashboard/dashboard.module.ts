import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { MainComponent } from './main/main.component';
import { AuthSharedModule } from '../auth/auth-shared/auth-shared.module';


@NgModule({
  declarations: [
    HeaderComponent,
    HomeComponent,
    MainComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    AuthSharedModule
  ],
  exports:[
    HomeComponent
  ]
})
export class DashboardModule { }
