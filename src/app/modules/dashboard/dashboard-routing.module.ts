import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessDeniedComponent } from '../auth/access-denied/access-denied.component';
import { Authority } from '../auth/constants/authorities';
import { ProyectoCreateComponent } from '../proyecto/proyecto-create/proyecto-create.component';
import { ProyectoListComponent } from '../proyecto/proyecto-list/proyecto-list.component';
import { ProyectoUpdateComponent } from '../proyecto/proyecto-update/proyecto-update.component';
import { RolsListComponent } from '../rols/rols-list/rols-list.component';
import { TareaCreateComponent } from '../tarea/tarea-create/tarea-create.component';
import { TareaListComponent } from '../tarea/tarea-list/tarea-list.component';
import { TareaUpdateComponent } from '../tarea/tarea-update/tarea-update.component';
import { UsuarioCreateComponent } from '../usuario/usuario-create/usuario-create.component';
import { UsuarioListComponent } from '../usuario/usuario-list/usuario-list.component';
import { UsuarioUpdateComponent } from '../usuario/usuario-update/usuario-update.component';
import { HomeComponent } from './home/home.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {
    path: '',
    /* data: {
      authorities: [Authority.ROLE_ADMINISTRADOR, Authority.ROLE_LIDER, Authority.ROLE_REGISTRADO]
    },
    canActivate: [UserRouteAccessGuard], */
    component: MainComponent,
    children: [
      {
        path: "usuario-list",
        data: {
          authorities: [Authority.ROLE_ADMINISTRADOR]
        },
        component: UsuarioListComponent
      },
      {
        path: "rols-list",
        component: RolsListComponent
      },
      {
        path: "proyecto-list",
        component: ProyectoListComponent
      },
      {
        path: "tarea-list",
        component: TareaListComponent
      },
      {
        path: 'tarea-create',
        component: TareaCreateComponent
      },
      {
        path: 'tarea-update/:id',
        component: TareaUpdateComponent
      },
      {
        path: 'proyecto-create',
        component: ProyectoCreateComponent
      },
      {
        path: 'proyecto-update/:id',
        component: ProyectoUpdateComponent
      },
      {
        path: 'usuario-create',
        data: {
          authorities: [Authority.ROLE_ADMINISTRADOR]
        },
        component: UsuarioCreateComponent
      },
      {
        path: 'usuario-update/:id',
        data: {
          authorities: [Authority.ROLE_ADMINISTRADOR]
        },
        component: UsuarioUpdateComponent
      },
      {
        path: 'tarea',
        loadChildren: () => import('../tarea/tarea.module')
          .then(m => m.TareaModule)
      },
      {
        path: 'usuario',
        loadChildren: () => import('../usuario/usuario.module')
          .then(m => m.UsuarioModule)
      },
      {
        path: 'rols',
        loadChildren: () => import('../rols/rols.module')
          .then(m => m.RolsModule)
      },
      {
        path: 'proyecto',
        loadChildren: () => import('../proyecto/proyecto.module')
          .then(m => m.ProyectoModule)
      },

      {
        path: 'access-denied',
        component: AccessDeniedComponent
      },
      {
        path: '',
        data: {
          authorities: [Authority.ROLE_ADMINISTRADOR, Authority.ROLE_LIDER, Authority.ROLE_REGISTRADO]
        },
        component: HomeComponent
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
