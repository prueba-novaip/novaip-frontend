import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccessDeniedComponent } from './modules/auth/access-denied/access-denied.component';
import { AuthSharedModule } from './modules/auth/auth-shared/auth-shared.module';
import { AuthInterceptor } from './modules/auth/guard/auth.interceptor';
import { LoginComponent } from './modules/auth/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    AccessDeniedComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AuthSharedModule,
    ReactiveFormsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
   }],
  bootstrap: [AppComponent]
})
export class AppModule { }
