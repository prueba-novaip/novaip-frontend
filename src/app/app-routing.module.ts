import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessDeniedComponent } from './modules/auth/access-denied/access-denied.component';
import { LoginComponent } from './modules/auth/login/login.component';

const routes: Routes = [
  {
    path:'dashboard',
    loadChildren:() => import ('./modules/dashboard/dashboard.module')
    .then(m =>m.DashboardModule)
  },
  {
    path:'access-denied',
    component:AccessDeniedComponent
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'',
    redirectTo:'login',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
